<?php

/* 
 * The MIT License
 *
 * Copyright 2016 lil.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if (!defined('_PS_VERSION_'))
	exit;

class mHighSlide extends Module
{	
	public function __construct()
	{
		$this->name = 'mhighslide';
 	 	$this->tab = 'front_office_features';
		$this->version = '0.0';
		$this->author = 'Lucian I. Last';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('m! HighSlide');
		$this->description = $this->l('Highslide JS is an image, media and gallery viewer written in JavaScript.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	
	public function install()
	{
		return(parent::install()
			&& $this->registerHook('header'));
	}
	
	public function uninstall()
	{
		return (parent::uninstall()
			&& $this->unregisterHook('header'));
	}
	
	public function hookHeader()
	{
//		$this->page_name = Dispatcher::getInstance()->getController();
//		if ($this->page_name == 'index')
//		{
			$this->context->controller->addJS(($this->_path).'js/mhighslide.js.php');
			$this->context->controller->addCss($this->_path.'css/mhighslide.css', 'all');
//		}
	}

}